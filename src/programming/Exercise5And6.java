package programming;

import java.util.List;

public class Exercise5And6 {
	public static void main(String args[]) {
		List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
		List<String> courses = List.of("Spring", "Spring Boot", "API", "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
		
		numbers.stream()
			.filter(number -> number % 2 != 0)
			.map(number -> number * number * number)
			.forEach(System.out::println);
		
		System.out.println("exercise 2");
		courses.stream()
			.map(course -> course.replace(" ", ""))
			.map(course -> course.length())
			.forEach(System.out::println);
	}
}
