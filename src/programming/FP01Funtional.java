package programming;

import java.util.List;

public class FP01Funtional {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
		
		//printAllNumberInListFunctional(numbers);
		//printEvenNumbersInListStructured(numbers);
		//printEvenNumbersInListFunctional(numbers);
		printSquaresOfEvenNumbersInListFunctional(numbers);
		
	}
	
	private static void print(int number) {
		System.out.println(number);
	}
	
	private static boolean isEven(int number) {
		return number %2 ==0;
	}
	private static void printAllNumberInListFunctional(List<Integer> numbers) {
		//What to do? New way of thinking!
			//What is happening below is that we are converting the LIST into STREAM
			//Once converted, we then put it in forEach()
			//In which, we are calling a method in FP01Functional class called print and throw each of the result
		numbers.stream()
			.forEach(FP01Funtional::print);// :: is called a Method Reference
		
		numbers.stream()
			.forEach(System.out::println); // Same idea - utilizing System.out class and call println method
		
		//How to loop the numbers? Old way of thinking!
		for(int number: numbers) {
			System.out.println(number);
		}
	}
	
	private static void printEvenNumbersInListStructured(List<Integer> numbers) {
		//How to loop? Old way of thinking!
		for(int number:numbers) {
			if(number % 2 == 0) {
				System.out.println(number);
			}
		}
	}
	
	private static void printEvenNumbersInListFunctional(List<Integer> numbers) {
		//what to do? New way of thinking!
		numbers.stream()
			//.filter(FP01Funtional::isEven)
			.filter(number -> number % 2 == 0 )
			.forEach(System.out::println);
	}
	
	private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers) {
		numbers.stream()
		.filter(number -> number % 2 == 0)
		.map(number -> number * number)
		.forEach(System.out::println);
	}
}
